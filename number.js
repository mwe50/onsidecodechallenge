var wordToSingle = {
   zero: 0,
   one: 1,
   two: 2,
   three: 3,
   four: 4,
   five: 5,
   six: 6,
   seven: 7,
   eight: 8,
   nine: 9,
   ten: 10,
   eleven: 11,
   twelve: 12,
   thirteen: 13,
   fourteen: 14,
   fifteen: 15,
   sixteen: 16,
   seventeen: 17,
   eighteen: 18,
   nineteen: 19,
   twenty: 20,
   thirty: 30,
   forty: 40,
   fourty: 40,
   fifty: 50,
   sixty: 60,
   seventy: 70,
   eighty: 80,
   ninety: 90,
};

var wordToMultiplier = {
   hundred: 100,
   thousand: 1000,
   million: 1000000,
   billion: 1000000000,
   trillion: 1000000000000,
};

var singleToWord = inverse(wordToSingle);

var multiplierToWord = inverse(wordToMultiplier);

/**
 * Reverese an objects key and value pairs
 *
 * @param {Object} obj
 * @returns {Object} reversed obj
 */
function inverse(obj) {
   var retobj = {};
   for (var key in obj) {
      retobj[obj[key]] = key;
   }
   return retobj;
}

/**
 * Converts english words to numbers in decimal notation
 *
 * @param {String} string
 * @returns {int} number representation of words
 */
function convertToNumber(string) {
   //Generate a string of words
   string = string.replace(',', '');
   var words = string.split(' ');

   //Create an array, index 0 is dollars index 1 is cents
   var amount = [0, 0];
   var level = 0;

   //Word index
   var i = 0;
   while (i < words.length) {
      //Attempt to read a single block of numbers, i.e one hundred thousand
      var block = 0;

      //Shift to cents after 'dollars' is read
      if (words[i] == 'dollars' || words[i] == 'dollar') {
         level = 1;
      }

      //If word is a number
      if (words[i] in wordToSingle) {
         block = wordToSingle[words[i]];

         //Loop through then next numbers to get the whole block
         i += 1;
         var chaining = false; //Single numbers must be added, however if we are chaining multipliers, we need to be sure not to chain a single
         while (i < words.length) {
            if (words[i] == 'and') {
               //Allows a single to be added to the current chain, while remaining in block
               chaining = false;
            } else if (words[i] in wordToMultiplier) {
               block *= wordToMultiplier[words[i]];
               chaining = true;
            } else if (words[i] in wordToSingle && !chaining) {
               block += wordToSingle[words[i]];
            } else {
               //Not recognised word or single without 'and', block ends
               break;
            }

            i += 1;
         }

         //Add block to total
         amount[level] += block;
      } else {
         //Word not recognised, move on
         i += 1;
      }
   }

   //Combine and return dollars + cents
   return amount[0] + amount[1] / 100;
}

/**
 * Converts a number in decimal form to english words
 *
 * @param {String} dollars
 * @returns {String} words
 */
function convertToString(dollars) {
   //Split parseInto dollars and cents
   var dollars = dollars.toString();
   if (dollars.includes('.')) {
      temp = dollars.split('.');
      dollars = temp[0];
      cents = temp[1];
   } else {
      cents = '';
   }

   var sentence = '';

   //Start with first numbers that are not in a group of 3
   var indexStart = 0;
   var indexEnd = dollars.length % 3 != 0 ? dollars.length % 3 : 3;

   //Calculate how many groups there are
   var loops = Math.ceil(dollars.length / 3) - 1;

   //Loop through all groups
   while (indexEnd <= dollars.length) {
      //Power reprents the 100 - 1 didgits
      var power = indexEnd - indexStart - 1;

      //Loop through all didgits in the group
      while (indexStart < indexEnd) {
         //Skip if 0
         if (dollars[indexStart] != '0') {
            //If one
            if (power == 0) {
               //If nothing before it in the group
               if (
                  indexStart > 2 &&
                  dollars[indexStart - 1] == '0' &&
                  dollars[indexStart - 2] == '0'
               ) {
                  sentence += ' and';
               }

               sentence += ' ' + singleToWord[parseInt(dollars[indexStart])];
            }
            //If ten
            if (power == 1) {
               //No number after the tens
               if (dollars[indexStart + 1] == '0') {
                  if (dollars[indexStart - 1] != '0' && indexStart > 0) {
                     sentence += ' and';
                  }
                  if (dollars[indexStart] != '1') {
                     sentence +=
                        ' ' + singleToWord[parseInt(dollars[indexStart]) * 10];
                  } else {
                     sentence +=
                        ' ' +
                        singleToWord[
                           parseInt(dollars[indexStart]) * 10 +
                              parseInt(dollars[indexStart + 1])
                        ];
                  }
               } else {
                  //Tens
                  if (dollars[indexStart] != '1') {
                     sentence +=
                        ' ' + singleToWord[parseInt(dollars[indexStart]) * 10];
                     sentence +=
                        ' ' + singleToWord[parseInt(dollars[indexStart + 1])];
                  }
                  //Teen
                  else if (dollars[indexStart] == '1') {
                     sentence +=
                        ' ' +
                        singleToWord[
                           parseInt(dollars[indexStart]) * 10 +
                              parseInt(dollars[indexStart + 1])
                        ];
                  }
                  //Zero
                  else {
                     sentence +=
                        ' ' + singleToWord[parseInt(dollars[indexStart + 1])];
                  }
               }
               indexStart += 1;
            }
            //If hundred
            if (power == 2) {
               sentence += ' ' + singleToWord[parseInt(dollars[indexStart])];
               sentence += ' ' + multiplierToWord[100];

               //If extra numbers in group
               if (
                  dollars[indexStart + 1] != '0' ||
                  dollars[indexStart + 2] != '0'
               ) {
                  sentence += ' and';
               }
            }
         }
         indexStart += 1;
         power -= 1;
      }

      //Add comma on conditions
      if (loops > 0) {
         sentence += ' ' + multiplierToWord[1000 ** loops];

         //Check that there are more numbers to come
         if (dollars.slice(indexEnd).replace('0', '') != '') {
            sentence += ',';
         }
      }

      loops -= 1;
      indexEnd += 3;
   }

   sentence += ' dollars';

   //Calculate cents------------------------------------------------------
   power = 1;
   indexStart = 0;
   indexEnd = cents.length;
   while (indexStart < indexEnd) {
      sentence += ' and';
      //Skip if 0
      if (cents[indexStart] != '0') {
         //If one
         if (power == 0) {
            sentence += ' ' + singleToWord[parseInt(cents[indexStart])];
         }

         //If ten
         if (power == 1) {
            //No number after the tens
            if (cents[indexStart + 1] == '0') {
               if (cents[indexStart] != '1') {
                  sentence +=
                     ' ' + singleToWord[parseInt(cents[indexStart]) * 10];
               } else {
                  sentence +=
                     ' ' + singleToWord[parseInt(cents[indexStart]) * 10];
               }
            } else {
               //Tens
               if (cents[indexStart] != '1') {
                  sentence +=
                     ' ' + singleToWord[parseInt(cents[indexStart]) * 10];
                  sentence +=
                     ' ' + singleToWord[parseInt(cents[indexStart + 1])];
               }
               //Teen
               else if (cents[indexStart] == '1') {
                  sentence +=
                     ' ' +
                     singleToWord[
                        parseInt(cents[indexStart]) * 10 +
                           parseInt(cents[indexStart + 1])
                     ];
               }
               //Zero
               else {
                  sentence +=
                     ' ' + singleToWord[parseInt(cents[indexStart + 1])];
               }
            }
            indexStart += 1;
         }
      }
      indexStart += 1;
      power -= 1;
   }
   //Add 'cents' to string
   sentence += indexEnd != 0 ? ' cents' : '';

   //Remove accidental spaces and return
   return sentence.trim();
}
